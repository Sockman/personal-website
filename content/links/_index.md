---
title: "Links"

---

## Works
- {{< link_blank "Bandcamp" "https://julianheuser.bandcamp.com/" >}}
- {{< link_blank "YouTube" "https://www.youtube.com/c/Sockman1" >}}
- {{< link_blank "Itch.io" "https://sockman.itch.io" >}}
- {{< link_blank "ModDB" "https://www.moddb.com/members/sockman1" >}}
- {{< link_blank "Steam (developer)" "https://store.steampowered.com/search/?developer=Julian%20Heuser" >}}
## Professional
- {{< link_blank "Linkedin" "https://www.linkedin.com/in/julian-heuser" >}}
- {{< link_blank "GitLab" "https://gitlab.com/Sockman" >}}
- {{< link_blank "GitHub" "https://github.com/JulianHeuser" >}}
- {{< link_blank "Bluesky" "https://bsky.app/profile/julianh.dev" >}}
- {{< rawhtml >}} <!-- Obfuscation--> <script>document.write('<a class="cryptedmail" href="m' + atob("YWlsdG86anVsaWE=") + 'nheuser' + atob("QG91dGxvb2suY29t") + '">Em' + 'ail</a>')</script> {{< /rawhtml >}}

## Personal
- {{< rawhtml >}} <a rel="me" href = "https://mas.to/@julianh" target="_blank">Mastodon</a> {{< /rawhtml >}}
- {{< link_blank "Steam (personal)" "https://steamcommunity.com/id/sockman101/" >}}

## Other
(This is mostly for the sake of completeness. I can't promise that anything here is relevant.)
- {{< link_blank "Scratch" "https://scratch.mit.edu/users/sockman101/" >}}