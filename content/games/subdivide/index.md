---
title: SubDivide
links: [["Steam", "https://store.steampowered.com/app/1292070/SubDivide/Steam"], ["Itch.io", "https://sockman.itch.io/subdivide"]]
---


SubDivide is a game I made between 2019 and 2021. The idea came from a [very incomplete game I made in 2 days for a game jam.](https://sockman.itch.io/tick-tock) 

The art was created by Katherine Yu. I did all the programming, [audio](https://julianheuser.bandcamp.com/album/subdivide-ost), and design.

## Gameplay
{{< rawhtml >}}

{{< video src="portfolio/SubDivide_audio.webm" controls="true" >}}

{{< /rawhtml >}}

## Music
{{< rawhtml >}}
<iframe class="bandcamp" src="https://bandcamp.com/EmbeddedPlayer/album=1195734717/size=large/bgcol=333333/linkcol=4ec5ec/artwork=small/transparent=true/"></iframe>
{{< /rawhtml >}}