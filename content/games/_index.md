---
title: "Games"

categories: ["Games"]
---

Most of my released and upcoming games. I've been developing games since at least 2015, when I released my [first game](https://www.moddb.com/games/rampant). Since then, I've participated in numerous game jams, and developed a few long-term solo projects. I used to primarily use Unity, but now use Godot as my game engine of choice.