---
layout: "blog"
title: "Year Unknown: Devlog 1"
date: 2024-02-17 00:00:00
draft: false
tags: ["games", "game dev", "year unknown"]
---

(This is written version of my video, which you can watch [here](https://www.youtube.com/watch?v=BZRZro8cRwA))

Before I even finished my last game, SubDivide, I knew that for my next big project I wanted to do something with narrative - something I unfortunately had to push aside during SubDivide's development. I had some ideas for the story but what I needed was an interesting way to tell that story. I didn't want to just make another RPG, visual novel, or walking simulator. Nothing against those genres - I just wanted to see if there was something more unique I could do.

![Art for my game, SubDivide](subdivide.jpeg)

My first idea involved what I called "books". These "books" would store some information about something, and to read it you had to plug it into a terminal.

{{< rawhtml >}}
<video src="prototype_1.mp4" type="video/mp4" controls></video>
{{< /rawhtml >}}

This was kind of interesting - making these abstract bits of info into physical things made them a lot more fun to interact with. But you can probably see why this hit a dead end. Mechanically, it's really no different from just collecting something and reading it. It might make a cool addition to a larger game, but it couldn't really stand on it's own.

So, logically, if something doesn't work, why not do more of it? So, I put two slots on the terminal. The idea was that each book could contain information on a certain topic, and putting them together would give you some information regarding the overlap of those two topics.

You can probably see the problems with this too. Accounting for every combination would be a lot of work, and I couldn't think of many uses for this anyway. It was just way too much work for something that really wasn't interesting in the first place.

So after these two prototypes fell kind of flat, I went back to the drawing board. Or, rather, I remembered that I played Outer Wilds and that it integraded story and gameplay far better than I could ever do.

(Seriously, go play Outer Wilds.)

![Reading text in Outer Wilds](https://cdn.akamai.steamstatic.com/steam/apps/753640/ss_09f0fa8d9b8d7da1408cf4e03303d896cbd9be18.600x338.jpg)
*Source: Outer Wilds, Steam*

But, maybe there was something I could take from it. Instead of trying to beat Outer Wilds at its own game, maybe I could do something that it doesn't do. And one thing Outer Wilds doesn't have that much of is immediate story. What I mean is that, most of Outer Wilds is about figuring out past events, events that are fixed in place. You don't have much in the way of meaningful dialogue options that change the outcome of the story - I mean, there are multiple endings, but only one real ending.

So, what if I could find a way to tell a story that happened in the past, but in the process, also develop a story that's ongoing, and that the player has agency to change?

I realize that sentence is pretty convoluted, so I'll break it down: I want the discovery and exploration of Outer Wilds, in tandem with the choice-based stuff of and RPG or something like a Telltale game.

I ended up keeping the terminals from the previous prototypes, they give an easy way to provide story through text, and act as good points of interest.

But instead of the "books" having information, each terminal would have it's own information on it. Except, the player can't access it directly. Instead, there's an AI that can. (Not an actual AI, their dialogue is written by me, they're an AI in the story. When I first wrote this dev log I didn't have to specify that...) This AI can see the information on the terminals directly, and for the player to get the information, they have to talk to the AI first.

Since the player is conversing with a character, this lets me add choice. But in the process, the player gets to uncover information.
And I could even go further than that - what if I add another AI, and give the player the option between the two? Now, to really get at the truth, the player has to take into account each AI's biases and motivations. For instance, what if only one of  them reveals an important fact. Are they lying, or is the other omitting the truth?

{{< rawhtml >}}
<video src="prototype_2.mp4" type="video/mp4" controls></video>
{{< /rawhtml >}}

This idea had a lot of potential - so I went with it.

This wasn't the end though. My first iteration on this idea sucked. It was completely linear so the choices weren't really coming into play, but the worse issue was that I had no way to gate the player's progress - they could just walk past a terminal, completely skipping important information. I had to make sure they get the information they need without putting a locked door after every terminal.

To solve this, I switched to a more open structure, with several areas that have one big gate or bottleneck in them to ensure the player knows enough for the rest of the game. This gave the player a bit more freedom to explore, and makes writing a lot easier for me, since I don't have to keep adding different dialogue branches in case the player misses something important.

For now, I'm pretty happy with this concept. The game of course still needs a lot of work, but hopefully no major restructuring at this point.
