---
layout: "blog"
title: "Year Unknown: Devlog 2"
date: 2024-02-17 00:00:01
draft: false
tags: ["games", "game dev", "year unknown"]
---

(This is written version of my video, which you can watch [here](https://www.youtube.com/watch?v=HOsSjW_SPyE))

Year Unknown has a lot of text. It's almost the entire game, so it's important that people can get invested in it. But writing is hard. And to make matters worse, all the text in my game is dialogue. Conveying emotion just through text is already hard enough, but with prose, you can at least just say what a character is feeling, or how something was said. Dialogue on its own can't convey any of this, unless you make things annoyingly obvious. If you've ever gotten a text from a friend and not known if they were being serious or not, you probably know what I mean.

What a lot of games do to help with this is add character portraits. These can show different expressions and poses for different characters, and can change depending on what they're currently saying. It's a great way to act out a scene if you don't have the budget for fully animated cut-scenes. I've been thinking about adding these for a while, and now I've finally gone ahead and implemented it.

![Example of a character portrait during dialogue from the game Celeste](celeste-dialogue.jpg)
*Source: Celeste, https://interfaceingame.com/*

For the design, I'm going with something simple for now. A simple circle is about as minimalist as I can go while still be able to convey different expressions.

To get it to work, I could just create animations, but then I'd have to manually make animations from every expression to every other expression. Not to mention the complexity of handling all those transitions.

Instead, I can do everything algorithmically. Fortunately, I'm using Godot, which has built-in tools for bezier curves. It's easy to represent a circle with a bezier curve using four control points. (Or, rather, five in this case to close the shape.) Moving each control point gives us a lot of control. For example, moving the lower control point up makes it look happy, moving the top one down makes it look angry. We can even move the tangents to show an even wider variety of expressions.

![Example of a character portrait during dialogue from the game Celeste](example_1.png)
*An angry bezier curve*

I can store an array of bezier curves for each expression I need. The index matches an enum specified in my dialogue manager. Then, I have one more bezier curve stored in a variable. This is the “true” curve that's displayed to the player. I have another variable that determines the index of the target expression. Then, each frame, I loop through each control point and handle in the true curve, and lerp their positions to the corresponding positions of the target curve.

[You can view the code here](code.txt)

This lerp every frame is a handy trick I like to use a lot. Basically, if I set the lerp to something like .5, it will go to the midpoint each frame. Since the start position is being updated, the midpoint updates each frame as well. This results is a smooth damping as it approaches the target position. And because of its simplicity, it doesn't matter what the target is set to. Even if it's midway through a transition, and the expression changes, it will just carry on and smoothly transition to that new target.

So far, it looks like… nothing. The bezier curve is being updated, but that's just a data type. It's visible in the editor, but not in-game. Again though, Godot is a good game engine, so there's a nice solution for this. There's a Polygon2D node that can render a series of points as a closed polygon. [Godot has a function](https://docs.godotengine.org/en/4.2/classes/class_curve2d.html#class-curve2d-method-get-baked-points) to bake the bezier curve into a series of points, and [another function](https://docs.godotengine.org/en/4.2/classes/class_polygon2d.html#class-polygon2d-property-polygon) to set the points of a Polygon2D node. And both of these use the same data type for the point array. This means all I have to do is set the Polygon2D's points to the bezier curve's points, and we're done!

I connected this up to my dialogue system, and it worked great. It was a bit bland though, so I added some idle animations on top of everything. This was just done with Godot's animation system to change the position of the Polygon2D. I could do this since the animations using the bezier curve was all in the points - the actual transform of the node was left alone.

{{< rawhtml >}}
<video src="showcase.mp4" type="video/mp4" controls></video>
{{< /rawhtml >}}

My only concern is performance, since I'm re-baking the bezier curve every frame. But unlike the terminal screens themselves, these won't be active until you actually interact with the terminal. So I only have to have one of these, and set it to display at whatever terminal the player is using. It may still be a bit of a performance loss, but I couldn't notice a difference.

In the end, this ended up being a really great change - it adds a bit of liveliness to the characters that I feel was missing before. Plus, it's really easy to use - I just have to select the expression I want for each chunk of dialogue.
