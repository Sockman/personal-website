---
layout: "blog"
title: "Vertical Music in Portal 2"
date: 2023-09-08
draft: false
tags: ["games", "music"]
---

Last time we talked about horizontal resequencing, so this time we'll talk about it's complement: vertical reorchestration. To best understand vertical reorchestration, let's look at the staff from the last post again:

![An image of a music staff](../horizontal-music/musicStaff.png)

Different instruments are laid out from top to bottom (vertically), so vertical reorchestration is simply adding or removing these different instruments, or tracks, over time. A simple example of this is in *Super Mario World*, where riding Yoshi enables accompanying bongos. Note that the original music is still playing, there's just another layer added on top of it.


{{< rawhtml >}}
<video src="smw.mp4" type="video/mp4" controls></video>
{{< /rawhtml >}}


For a more complex example, the game we'll look at is Portal 2. Portal 2 has a lot of dynamic music, including horizontal resequencing and even some generative music (which we'll get to later!). But vertical reorchestration is probably its most utilized technique.


To start off, let's look at an example from test chamber 10, one of the earlier puzzles in the game. This includes quite a few mechanics, including lasers, aerial faith plates which launch the player in the air, and cubes with buttons. Watch this walkthrough and listen carefully (although feel free to skip around if you're in a rush):


{{< rawhtml >}}
<iframe src="https://www.youtube.com/embed/izx6HBa-Tow"
    title="YouTube video player" frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    class="youtube_video" allowfullscreen></iframe>
{{< /rawhtml >}}


There's a lot going on here so let's break it down. (Headphone warning! Some of these are quite loud and
harsh-sounding.)


First, we have the base layer. This starts playing when you enter the chamber and keeps playing until it ends.


{{< rawhtml >}}
<audio controls src="sp_a2_ricochet_b1_01.mp3" class="audioExample"></audio>
{{< /rawhtml >}}

We have this track playing around the area where you pick up the first cube:

{{< rawhtml >}}
<audio controls src="sp_a2_ricochet_x1.mp3" class="audioExample"></audio>
{{< /rawhtml >}}

Then a track for when the first laser receiver is activated:

{{< rawhtml >}}
<audio controls src="sp_a2_ricochet_l1_01.mp3" class="audioExample"></audio>
{{< /rawhtml >}}

Then a track for when the second laser receiver is activated:

{{< rawhtml >}}
<audio controls src="sp_a2_ricochet_l2_01.mp3" class="audioExample"></audio>
{{< /rawhtml >}}

And lastly, we have a few tracks that play when you launch something using the aerial faith plates:

{{< rawhtml >}}
<audio controls src="sp_a2_ricochet_c2_01.mp3" class="audioExample"> </audio>
<audio controls src="sp_a2_ricochet_c3_01b.mp3" class="audioExample"> </audio>
{{< /rawhtml >}}

If you want to mess around a bit, I've added a few buttons to make them all play in-sync. Then you can mess
with the volumes to fade things in and out. They won't necessarily stay in sync for long, as in-game it's a
bit more complex than this. But it's a good way to get a feel for how each part fits together.

{{< rawhtml >}}
<button id="startExample">Start all in sync (watch your volume!)</button>
<button id="endExample">Stop all</button>
{{< /rawhtml >}}

There are even more tracks being used, but this is more than enough to show how far vertical reorchestration can go. As you progress through the test, you'll hear more and more of these tracks. And on top of that, all of these tracks are emitted from points in space, meaning that as you move and fling yourself around the chamber, you'll hear some of these tracks naturally fade in and out as you get closer or further away from where they're being emitted.

Which leads to perhaps the most interesting thing that this soundtrack does. Most music for film and games is non-diegetic, meaning that it's purely for the audience, and that the music isn't actually playing in that world. However, Portal 2's soundtrack is written to be diegetic. The soundtrack is written by composer Mike Morasky, but is credited to the “Aperture Science Psychoacoustics Laboratories.” This isn't because of some licensing problem, it was a decision made by the composer himself because of how he wrote the music. The guiding principle is that all of it is being created by the facility and is in-universe, and in turn much of it acts as sound effects. By treating all the music as diegetic, it ends up playing a key role in the game's puzzles. The two lasers in this test chamber are a perfect example of this. Each play a unique track when activated, which makes them easily identifiable. If they just used a sound effect they would be indistinguishable, not to mention it would make the chamber a bit less compelling.

In Morasky's own words, ["Overall the interactive musical experience tends to become one of exploration, as if the music is emanating from the facility and devices of Aperture Science itself, often giving the player total control over how much or little they want to expand that aspect of the experience."](https://www.gamesradar.com/portal-2s-dynamic-music-an-interview-with-composer-mike-morasky-and-five-tracks-to-listen-to-now/) Once you've solved the puzzle and launch yourself from the start to the end, you hear all the interweaving parts fading in and out as you fly by lasers and get launched by aerial faith plates. It acts as a musical reward for solving the puzzle, and an excellent example of how vertical reorchestration can enhance a game.

{{< rawhtml >}}
<script type="text/javascript">
    var audioSources = document.getElementsByClassName("audioExample");
    var buttonStart = document.getElementById("startExample")
    var buttonEnd = document.getElementById("endExample")

    buttonStart.onclick = function () {
        for (i = 0; i < audioSources.length; i++) {
            audioSources[i].currentTime = 0;
            audioSources[i].loop = true;
            audioSources[i].play();
        }
    };

    buttonEnd.onclick = function () {
        for (i = 0; i < audioSources.length; i++) {
            audioSources[i].loop = false;
            audioSources[i].pause();
        }
    };
</script>
{{< /rawhtml >}}
