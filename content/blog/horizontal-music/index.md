---
layout: "blog"
title: "Horizontal Music in Monkey Island 2"
date: 2023-09-08
draft: false
tags: ["games", "music"]
---

Video games and music go hand-in-hand. Try to think of a game released in the last 20 years without some form of music and I bet it will take you some time, if you can think of one at all.

Movies and tv shows have a similar connection to music, but what makes video games unique is that the music in them has to be dynamic. While a movie composer can write for a specific moment, and time the music perfectly, a video game composer has no idea when the player will do any given action. Because of that, composers have to get creative. This blog will explore some of the different approaches composers and developers take with various games to make the music feel in-sync with the player's actions.

Let's start with the simple stuff first. Perhaps the easiest way to make music “dynamic” is to just make a loop. This is what most games do and it's an easy way to ensure that music will play indefinitely. While there's nothing wrong with this approach in moderation, it can lead to some pretty annoying repetition, and it doesn't really allow for much variation.

This is where horizontal resequencing and vertical reorchestration come in. These are the two fundamental basics of dynamic music, and despite the long words they're very easy to understand. The names come from how music is generally laid out:

![Image of a music staff](musicStaff.png)

From left to right (horizontally), different notes are laid out over time. So, horizontal resequencing is just transitioning between different segments of music, and will be what we focus on for this first post.

The simplest use of horizontal resequencing is cuts. The original <i>Super Mario Bros.</i>, for example, just cuts the music out as soon as you end a level, before starting the level end theme. As hardware improved, crossfades could be used instead to make transitions less jarring. Sticking with Mario games, *Super Mario 64* fades any background music out when you enter a level.

{{< rawhtml >}}
<video src="mario64.mp4" type="video/mp4" controls></video>
{{< /rawhtml >}}

Something else this game does is change music based on location. Outside the castle, when you first
start the game, only ambient sounds are present. Entering the castle triggers the castle theme to
start playing until you leave it again.

This is all very basic stuff, and I could sit here all day listing examples, as almost every game has some sort of fade or transition between music. But I want to point out one particularly impressive example from the classic PC game *The Secret of Monkey Island 2: LeChuck's Revenge*. Listen carefully to this example:

{{< rawhtml >}}
<iframe src="https://www.youtube.com/embed/7N41TEcjcvM" title="YouTube video player" frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    class="youtube_video" allowfullscreen></iframe>
{{< /rawhtml >}}

Just like in Mario 64, the music transitions as the player enters different areas. But this time, there's no crossfade or jarring cut. It's all completely seamless. On top of that, this game came out in 1993, three years before Mario 64, and there were no software solutions yet to help make dynamic music, like Wwise or Fmod. So how did they do it?

The developers created their own solution from scratch, which they called iMUSE. There's a lot of technical aspects to it but the gist is that the composers wrote a ton of transitions between music to be played at certain times in each song. Then, iMuse uses information about the game to determine what to queue up and play. Essentially, the game is constantly generating a playlist for itself, and rather than crossfading, a composed transition is played instead to make everything much more seamless. And all of it is based on the player's actions. If you want to know more about the finer details [this article does a good job covering it.](https://mixnmojo.com/features/sitefeatures/LucasArts-Secret-History-Monkey-Island-2-LeChucks-Revenge/9)

Next post we'll look into a particularly interesting example of vertical reorchestration in Portal 2.
