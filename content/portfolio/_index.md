---
title: "Portfolio"
---

I specialize in a few different things, so if you're interested in one specific skillset, find the appropriate page below:
{{< rawhtml >}} <div class="portfolio_links"> {{< /rawhtml >}}
{{< portfolio_link name="../graphics" bg="portfolio/Year_Unknown_graphics3.webm" true_fullscreen="true" >}} Graphics {{< /portfolio_link >}}
{{< portfolio_link name="../audio" bg="portfolio/Void_Tapes_audio.webm" true_fullscreen="true" >}} Audio {{< /portfolio_link >}}
{{< portfolio_link name="../ui" bg="portfolio/Year_Unknown_UI.webm" true_fullscreen="true" >}} UI {{< /portfolio_link >}}
{{< rawhtml >}} </div> {{< /rawhtml >}}

Or just enjoy a few examples of my work below:

![Screenshot of Year Unknown](https://julianh.dev/games/yearunknown/presskit/screenshots/Screenshot2.png)
{{< video src="portfolio/Year_Unknown_graphics3.webm" autoplay="true" loop="true" >}}

{{< video src="portfolio/Void_Tapes_audio.webm" controls="true" loop="true" >}}
![Forest Canopy]({{< resource src="portfolio/DVC_forest.jpg" >}}) 

{{< video src="portfolio/SubDivide_audio.webm" controls="true" loop="true" >}}
{{< video src="portfolio/Year_Unknown_graphics2.webm" autoplay="true" loop="true" >}}

{{< video src="portfolio/War_Room_audio.webm" controls="true" loop="true" >}}
{{< youtube src="Jhgk_6AJSNI" >}}
