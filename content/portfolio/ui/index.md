---
title: "UI"
---

## Year Unknown
For Year Unknown, I wanted to provide options for accessibility and graphics so as many people as possible could play.
The UI uses tiers of menu items, with the player always able to see the last menu. It supports full keyboard and controller navigation.

{{< video src="portfolio/Year_Unknown_UI.webm" autoplay="true" loop="true" >}}
![Screen in Year Unknown](https://julianh.dev/games/yearunknown/presskit/screenshots/Screenshot2.png)

Every slider has an indicator of its value, often with immediate feedback, like a sound for volume or an example of the font size. Options that aren't immediately clear have tool-tips on hover explaining what they do. Pressing escape or the back button on controller always takes you back to the previous menu, and every section has a reset button for the options on that screen. The most important thing is that if you don't need any of those options, the menu is minimal, and stays out of the way of the game.

## SubDivide
A precursor to the menu design in Year Unknown - the use of tiered menus was something I later improved on.

{{< video src="portfolio/SubDivide_UI.webm" autoplay="true" loop="true" >}}

## Professional Work
At {{< link_blank "Rochester Software Associates" "https://www.rocsoft.com/" >}}, I created several pages and components for {{< link_blank "WebCRD 12" "https://www.rocsoft.com/blogs/in-plant-insights/rochester-software-associates-releases-version-12-of-its-leading-webcrd-web-to-print-software/" >}}, which we were upgrading from Ajax to a new React frontend.

Below is a recreation of one of the designs I proposed and implemented, which made it into the final product. The original prototype just indicated the use of drag-and-drop to rearrange items, with no implementation details.

{{< rawhtml >}}
<style>
@keyframes card-in {
  from {
    overflow: hidden;
    width: 0;
    padding-left: 0;
    padding-right: 0;
    margin-right: 0;
    margin-left: 0;
  }

  to {
  }
}

@keyframes card-out {
  from {
    overflow: hidden;
  }

  to {
    overflow: hidden;
    width: 0;
    padding-left: 0;
    padding-right: 0;
    margin-right: 0;
    margin-left: 0;
  }
}

.example_container{
    height: auto;
}

.example_card_style{
    animation: card-in .2s forwards;

    width: 150px;
    height: 250px;
    border-radius: 5px;
    background-color: white;
    display: inline-block;
    margin: 5px;
    color: black;
    padding: 5px;
    vertical-align: top;
    text-align: center;
}
.example_card_badge{
    background-color: grey;
    border-radius: 10px;
    padding: 10px;
    width: 50px;
    transform: translate(-10px, -10px);
    line-height: 0;
}
.example_card_badge>input{
    width: 100%;
    appearance: textfield;
    background-color: unset;
    border-color: transparent;
    box-sizing: border-box;
    text-align: center;
    font-size: 1rem;
}
.example_card_badge>input:focus{
    appearance: revert;
    border-color: revert;
    background-color: revert;
}

.example_dragtarget{
    display: inline-block;
    height: 50px;
    width: 20px;
    background-color: white;
    opacity: 0;
    margin-top: 90px;
    border: 2px solid white;
}

.example_dragtarget_visible{
    opacity: 1;
}

.example_dragtarget_dragged{
    background-color: grey;
}

.card_destroyed{
    animation: card-out .2s forwards;
}

</style>

<div class="example_container">
<div class="example_card_style example_card" draggable="true" ondragstart="drag(event)" ondragend="dragend(event)" onkeydown="keydown(event)">
    <div class="example_card_badge"> <input type="number" onfocusout="getinput(event)"></input> </div>
    <span>Try dragging these cards to reorder them.</span>
</div>

<div class="example_card_style example_card" draggable="true" ondragstart="drag(event)" ondragend="dragend(event)" onkeydown="keydown(event)">
    <div class="example_card_badge"> <input type="number" onfocusout="getinput(event)"></input> </div>
    <span>You can also click the badge to type a position.</span>
</div>

<div class="example_card_style example_card" draggable="true" ondragstart="drag(event)" ondragend="dragend(event)" onkeydown="keydown(event)">
    <div class="example_card_badge"> <input type="number" onfocusout="getinput(event)"></input> </div>
    <span>The number inputs make this accessible and entirely usable with just a keyboard.</span>
</div>

<div class="example_card_style example_card" draggable="true" ondragstart="drag(event)" ondragend="dragend(event)" onkeydown="keydown(event)">
    <div class="example_card_badge"> <input type="number" onfocusout="getinput(event)"></input> </div>
    <span>This was originally implemented in React, but this demo is made with pure JavaScript.</span>
</div>

<div class="example_card_style example_card" draggable="true" ondragstart="drag(event)" ondragend="dragend(event)" onkeydown="keydown(event)">
    <div class="example_card_badge"> <input type="number" onfocusout="getinput(event)"></input> </div>
    <span>The original didn't have animation. But I think it helps with clarity.</span>
</div>
</div>

<script type="text/javascript">
    let cards;
    let targets;

    let inputLock = false;

    function updateCards() {
        cards = document.getElementsByClassName("example_card");
        targets = document.getElementsByClassName("example_dragtarget");
        
        while(targets[0]) {
            targets[0].parentNode.removeChild(targets[0]);
        }

        let dragTarget = document.createElement("div");
        dragTarget.className = "example_dragtarget";

        let i = 1
        let targetReal = dragTarget.cloneNode(true);
        cards[0].parentNode.insertBefore(targetReal, cards[0])
        targetReal.ondragover = enterDrop;
        targetReal.ondragleave = exitDrop;
        targetReal.ondrop = drop;

        for (card of cards) {
            card.querySelector("input").value = i;
            targetReal = dragTarget.cloneNode(true);
            card.parentNode.insertBefore(targetReal, card.nextSibling)

            targetReal.ondragover = enterDrop;
            targetReal.ondragleave = exitDrop;
            targetReal.ondrop = drop;

            i++;
        }
    }

    updateCards();

    function drag(ev) {
        ev.dataTransfer.setData("id", ev.target.querySelector("input").value);
        for (target of targets) {
            target.classList.add("example_dragtarget_visible");
        }
    }

    function dragend(ev) {
        ev.preventDefault();
        for (target of targets) {
            target.classList.remove("example_dragtarget_visible");
        }
    }

    function enterDrop(ev) {
        ev.preventDefault();
        ev.target.classList.add("example_dragtarget_dragged");
    }

    function exitDrop(ev) {
        ev.preventDefault();
        ev.target.classList.remove("example_dragtarget_dragged");
    }

    function drop(ev) {
        ev.preventDefault();

        if (inputLock) return;
        inputLock = true;

        var data = ev.dataTransfer.getData("id");
        
        let selectedCard = cards[data - 1];
        let selectedTarget = ev.target;

        selectedTarget.parentNode.insertBefore(selectedCard.cloneNode(true), selectedTarget)
        
        selectedCard.classList.add("card_destroyed");
        selectedCard.classList.remove("example_card");
        setTimeout(() => { selectedCard.remove(); inputLock = false; }, 200)
        updateCards();
    }

    function keydown(ev) {
        if (ev.key === "Enter") {
            ev.preventDefault();
            getinput(ev);
        }
    }

    function getinput(ev) {
        ev.preventDefault();

        if (inputLock) return;
        inputLock = true;

        let selectedCard = ev.target.parentNode.parentNode;
        let cardIndex = [...cards].indexOf(selectedCard);

        let val = ev.target.value - 1;
        if (cardIndex < val) val++;
        val = Math.max(val, 0);
        val = Math.min(val, 5);

        let selectedTarget = targets[val];
        
        selectedTarget.parentNode.insertBefore(selectedCard.cloneNode(true), selectedTarget)

        selectedCard.classList.add("card_destroyed");
        selectedCard.classList.remove("example_card");
        setTimeout(() => { selectedCard.remove(); inputLock = false; }, 200)
        updateCards();
    }
</script>

{{< /rawhtml >}}

At {{< link_blank "Great Lakes Gaming" "https://www.glgaminglounges.com/" >}} I worked on an overlay for esports games, as well as a control app used by broadcasters. This used Unity and was mostly made of uGUI elements.

{{< youtube src="5XP4FwwTd8w" >}}