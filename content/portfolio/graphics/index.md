---
title: Graphics
---

## Year Unknown
A short narrative-focused game made with Godot, Blender, and GIMP. Shaders were a mix of visual shaders and Godot's shader language (based on GLSL).

The VFX below were made with Godot's GPU particle system. The lightning on the right was made with a 3D sprite, an emissive material, a light, and a script that quickly flips the sprite over the x and y axes.

{{< video src="portfolio/Year_Unknown_graphics1.webm" autoplay="true" loop="true" >}}
{{< video src="portfolio/Year_Unknown_graphics2.webm" autoplay="true" loop="true" >}}
I wanted the game to give a sense of megalophobia, but still run on limited hardware. I use baked lighting, with a clean material that uses tri-planar mapping to save me the trouble of UV unwrapping these massive scenes.

{{< video src="portfolio/Year_Unknown_graphics3.webm" autoplay="true" loop="true" >}}
![Screenshot of Year Unknown](https://julianh.dev/games/yearunknown/presskit/screenshots/Screenshot7.png)

Near the end I wanted a scene of a grassy field. This took some work to get performant on my min-spec target (my sibling's 5-year-old laptop). I used GPU instancing to render the grass, and used an opaque material to avoid overdraw. Godot's GPU instancing doesn't allow for per-instance culling, so I broke the grass up into smaller groups that the CPU could cull. The only cost was some extra draw calls, which the CPU had more than enough headroom to handle.

You can view the source code {{< link_blank "here" "https://gitlab.com/Sockman/year-unknown/-/blob/main/Models/Materials/Grass/grass_shader.tres" >}}.

## Professional Work

At {{< link_blank "Diamond Visionics" "https://diamondvisionics.com/" >}}, I worked on software which renders the out-the-window view for flight simulators.

I implemeneted a customer request to create a forest canopy to cover large areas, and is cheaper to render than placing down thousands of sprites. The solution I came up with randomly scatters vertices, connects them using a constrained delaunay triangulation algorithm, and randomizes the heights. To improve the look, I darkened lower areas to emulate ambient occlusion. Even with millions of vertices, this solution eliminates the expensive overdraw caused by sprites, making it far faster to render.

![Forest Canopy]({{< resource src="portfolio/DVC_forest.jpg" >}}) 

I also did work on a forest fire system. I started by implementing surface fires. These were a terrain-conforming effect, and utilized existing destruction features to "burn" the underlying terrain, forests, and other features. To make this easier, I added configuration options to attach effects and particle systems to destruction. While I didn't implement crown fires, they could easily be added as a particle system and attached alongside the surface fire.

![Forest Canopy]({{< resource src="portfolio/DVC_fire.jpg" >}})
{{< video src="portfolio/DVC_fire_2.webm" autoplay="true" loop="true" >}}

## Academics

For the final project for *Intro to Computer Graphics,* I created procedural terrain with WebGL. View the source code {{< link_blank "here" "https://github.com/JulianHeuser/intro-to-graphics-final" >}}.

![Intro to Graphics]({{< resource src="portfolio/Intro_Graphics.jpg" >}}) 
