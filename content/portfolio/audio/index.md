---
title: "Music and Sound Design"
---

Music Demo: {{< audio src="portfolio/DemoReel_Music.ogg" large="true" >}}

Sound Design Demo: {{< audio src="portfolio/DemoReel_SFX.ogg" large="true" >}}

## Year Unknown

Music was created in Reaper using mostly free VSTs, primarily Vital and {{< link_blank "Yoshimi" "https://en.wikipedia.org/wiki/Yoshimi_(synthesizer)" >}} (which is super janky but I love the sounds it makes).

I started by experimenting with the styles and sounds I wanted, before creating the motifs of the two main characters (AL and BO), and what I suppose is the game's main motif: the first five notes of a minor scale. It's uneven, unresolved, and makes it impossible to know what type of minor scale it is - matching the game's themes of existentialism, and questions with no good answer.

{{< rawhtml >}}
<iframe class="bandcamp" src="https://bandcamp.com/EmbeddedPlayer/album=1097177362/size=large/bgcol=333333/linkcol=0f91ff/artwork=small/transparent=true/" seamless><a href="https://julianheuser.bandcamp.com/album/year-unknown-ost">Year Unknown OST by Julian Heuser</a></iframe>
{{< /rawhtml >}}

{{< youtube src="Jhgk_6AJSNI" >}}

For the game's sound, I created a lot of ambience from manipulating recordings of fluorescent lights, air vents, or just quiet nights. I usually apply some reverb, stretching, and/or pitch shifting, but for a few sounds I used granular synthesis tools to get something that sounds like otherworldly machinery.

{{< video src="portfolio/Year_Unknown_audio1.webm" controls="true" >}}
{{< video src="portfolio/Year_Unknown_audio2.webm" controls="true" >}}

## War Room
A game jam game I created sound effects for. The game has a sanity system, so I created some spooky sounds that only play at low sanity, and increase in chance as it decreases.

{{< video src="portfolio/War_Room_audio.webm" controls="true" >}}

## Direct Current
Another game jam game I did the music for. You can play the game on {{< link_blank "itch.io" "https://sockman.itch.io/direct-current" >}}.

{{< rawhtml >}}
<iframe class="bandcamp" height="120px" src="https://bandcamp.com/EmbeddedPlayer/album=2954346832/size=large/bgcol=333333/linkcol=0f91ff/tracklist=false/artwork=none/track=1769980756/transparent=true/" seamless><a href="https://julianheuser.bandcamp.com/track/current">Gags, Remakes, and Miscellaneous Works by Julian Heuser</a></iframe>
{{< /rawhtml >}}

## Academics

### Void Tapes
Created as part of a graphics seminar at college, Void Tapes is an interactive music sandbox made in collaboration with artist {{< link_blank "Andromeda Bell" "https://open.spotify.com/artist/0UrMJd7H9MgCeSgCEcMh6O" >}}.

I created a {{< link_blank "realtime synth as a GDExtension" "https://github.com/JulianHeuser/interactive-music/tree/main/gd_extension_cpp/src" >}} in C++, which you can hear in the video to the left.

To sync each stem and the dynamic audio, I created an {{< link_blank "event handler to sync events to a global BPM" "https://github.com/JulianHeuser/interactive-music/blob/main/Music%20Management/MusicManager.gd" >}}. I used this to create a {{< link_blank "script to tie animations to audio" "https://github.com/JulianHeuser/interactive-music/blob/main/Music%20Management/audio_player_anim.gd" >}}, used for the animals producing music. Each animal is tied to a different stem.

{{< video src="portfolio/Void_Tapes_audio.webm" controls="true" >}}

### Story of a Sound

My final project for *Fundamentals of Audio Engineering*. This project shows the progression of sound from the air to our ears, and lets the user control a filter. Each "particle" produces part of a sound with a narrow band-pass filter. Not technically how sound works, but it produces a really cool effect.

{{< video src="portfolio/Story_of_a_Sound.webm" controls="true" >}}

## SubDivide

Music created in LMMS. Features a variety of styles to match the different environments of the game, and their different moods.

I created a dynamic music system using scriptable objects in Unity. It basically worked like a trimmed-down version of Wwise or Fmod, letting me dynamically crossfade between music on-tempo. You can hear the results in the video to the right, where level changes queue the music to start, then to transition to an outro. 

{{< rawhtml >}}
<iframe class="bandcamp" src="https://bandcamp.com/EmbeddedPlayer/album=1195734717/size=large/bgcol=333333/linkcol=4ec5ec/artwork=small/transparent=true/"><a href="https://julianheuser.bandcamp.com/album/subdivide-ost">SubDivide OST by Julian Heuser</a></iframe>
{{< /rawhtml >}}

{{< video src="portfolio/SubDivide_audio.webm" controls="true" >}}

