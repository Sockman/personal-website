---
title: "Mods"

categories: ["Mods"]
---

Various maps and mods I've created over the years, mostly for Source engine games.


You can play my maps for {{< link_blank "Half-Life 2" "https://steamcommunity.com/id/sockman101/myworkshopfiles/?appid=220" >}}, {{< link_blank "Half-Life: Alyx" "https://steamcommunity.com/id/sockman101/myworkshopfiles/?appid=546560" >}}, and {{< link_blank "Portal 2" "https://steamcommunity.com/id/sockman101/myworkshopfiles/?appid=620" >}} through the workshop, or see everything on the [archive](./maps).