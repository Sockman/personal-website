---
title: Frameless Labs Symposium Room Design
description: 
links: [["Room", "https://hubs.mozilla.com/Z67FaEm/modest-decisive-dominion"]]
---

[Frameless Labs](https://www.rit.edu/framelesslabs/) is an organization focused on VR, AR, and XR research and technology. Every year, they hold a symposium where presenters show demos, installations, performances, and research related to XR.

In 2020, due to Covid-19, the symposium was held online in Mozilla Hubs. A [contest](https://www.rit.edu/framelesslabs/symposium-2020#student-contest) was issued to design a 3D space for the symposium to be held in. The space I designed won first place, and was used for the symposium.

## Images

![Screenshot of a virtual 3D scene of a space station. View is within a large dome with windows.](frameless_screenshot_1.jpg)
![Screenshot of a virtual 3D scene of a space station. View is of a branching path to three small, circular rooms.](frameless_screenshot_2.jpg)
![Screenshot of a virtual 3D scene of a space station. View is of what looks like a lecture room, with seats and a large white screen at the front.](frameless_screenshot_3.jpg)
![Screenshot of a virtual 3D scene of a space station. View is of a circular room with a window viewing Earth.](frameless_screenshot_4.jpg)

## Design

To design the space I tried to account for every way it would be used. I have a large, central area for conversing which branches out to several rooms: a more conventional lecture hall with a screen, a smaller side room, and three even smaller breakout rooms. Mozilla hubs has spatial audio, so I took advantage of that with physically separate spaces. An added benefit of each room being unique is that navigation is a lot easier.

For aesthetics, I wanted something that took full advantage of being virtual, but wasn't so crazy that it was distracting. A space station was one of the first ideas that came to mind. The dark colors contrast with most user avatars. View of the Earth, Moon, and various asteroids add some visual spice to the background to give users something pleasant to look at. 
