---
title: "Julian Heuser"

categories: ["Games", "Mods", "Music", "Other"]
important_only: true
---

I'm a game developer and software developer specializing in graphics, audio, and UI/UX. I've worked on a wide range of projects (including [this website](https://gitlab.com/Sockman/personal-website)) which you can browse through below. You can also find my work on {{< link_blank  "Itch.io" "https://sockman.itch.io/" >}}, {{< link_blank "GitLab" "https://gitlab.com/Sockman" >}}, {{< link_blank "Bandcamp" "https://julianheuser.bandcamp.com/" >}}, and [more](links).

## Portfolio
{{< rawhtml >}} <div class="portfolio_links"> {{< /rawhtml >}}
{{< portfolio_link name="graphics" bg="portfolio/Year_Unknown_graphics3.webm" >}} Graphics {{< /portfolio_link >}}
{{< portfolio_link name="audio" bg="portfolio/Void_Tapes_audio.webm" >}} Audio {{< /portfolio_link >}}
{{< portfolio_link name="ui" bg="portfolio/Year_Unknown_UI.webm" >}} UI {{< /portfolio_link >}}
{{< rawhtml >}} </div> {{< /rawhtml >}}
