#!/bin/bash

ffmpeg -i "$1" -ss $2  -to $3 -s 1024x576 -r 30 -c:v libvpx-vp9 -b:v 1M -pass 1 -an -f null dev/null && \
ffmpeg -i "$1" -ss $2  -to $3 -s 1024x576 -r 30 -c:v libvpx-vp9 -b:v 1M -pass 2 -c:a libopus output.webm

